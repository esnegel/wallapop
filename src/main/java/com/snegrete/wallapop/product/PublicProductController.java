package com.snegrete.wallapop.product;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
@RequestMapping("/public")
public class PublicProductController {

	@Autowired
	private ProductService productService;
	
	@ModelAttribute(name="products")
	public List<Product> unsoldProducts(){
		return productService.unsoldProducts();
	}
	
	@GetMapping({"", "/", "/index"})
	public String index(Model model, @RequestParam(name="name", required=false) Optional<String> nameOpt) {
		nameOpt.ifPresent(name -> model.addAttribute("products", productService.unsoldProductsSearch(name)));
		return "index";
	}
	
	@GetMapping("/product/{id}")
	public String productById(Model model, @PathVariable Long id) {
		Optional<Product> productOpt = productService.getById(id);
		if (productOpt.isPresent()) {
			model.addAttribute("product", productOpt.get());
			return "/product";
		}
		return "redirect:/public";
	}
}
