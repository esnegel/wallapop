package com.snegrete.wallapop.product;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import com.snegrete.wallapop.filer.StorageService;
import com.snegrete.wallapop.user.WallapopUser;
import com.snegrete.wallapop.user.WallapopUserService;

@Controller
@RequestMapping("/app")
public class ProductController {

	@Autowired
	private ProductService productService;
	
	@Autowired
	private WallapopUserService wallapopUserService;
	
	@Autowired
	private StorageService storageService;
	
	private WallapopUser wallapopUser;
	
	@ModelAttribute(name = "userProducts")
	public List<Product> userProdcuts(){
		wallapopUser = wallapopUserService.findByEmail(SecurityContextHolder.getContext().getAuthentication().getName());
		return wallapopUser.getProducts();
	}
	
	@GetMapping("/my-products")
	public String myProducts(Model model, @RequestParam(name="name", required=false) Optional<String> nameOpt) {
		nameOpt.ifPresent(name -> model.addAttribute("userProducts", productService.userProductsSearch(name, wallapopUser)));
		return "/app/product/product-list";
	}
	
	@GetMapping("/product/new")
	public String newProduct(Model model) {
		model.addAttribute("product", new Product());
		return "/app/product/form";
	}
	
	@PostMapping("/product/new")
	public String newProduct(Model model, @ModelAttribute Product product, 
			@RequestParam(name = "file", required = false) Optional<MultipartFile> fileOpt) {
		fileOpt.ifPresent(file -> product.setImage(storageService.store(file)));
		product.setUser(wallapopUser);
		productService.save(product);
		return "redirect:/app/my-products";
	}
	
	@GetMapping("/product/{id}/delete")
	public String deleteProduct(@PathVariable Long id) {
		productService.delete(productService.getById(id).get());
		return "redirect:/app/my-products";
	}
	
	@GetMapping("/product/edit/{id}")
	public String editProduct(Model model, @PathVariable Long id) {
		model.addAttribute("product", productService.getById(id).get());
		return "/app/product/form";
	}
	
	@PostMapping("/product/edit")
	public String editProduct(Model model, @ModelAttribute Product product, 
			@RequestParam(name = "file", required = false) Optional<MultipartFile> fileOpt) {
		fileOpt.ifPresent(file -> product.setImage(storageService.store(file)));
		product.setUser(wallapopUser);
		productService.save(product);
		return "redirect:/app/my-products";
	}
}
