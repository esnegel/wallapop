package com.snegrete.wallapop.product;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.snegrete.wallapop.filer.StorageService;
import com.snegrete.wallapop.pruchase.Purchase;
import com.snegrete.wallapop.user.WallapopUser;

@Service
public class ProductService {

	@Autowired
	private ProductRepository productRepository;
	
	@Autowired
	private StorageService storageService;
	
	public Product save(Product product) {
		return productRepository.save(product);
	}
	
	public void delete(Product product) {
		storageService.delete(product.getImage());
		productRepository.delete(product);
	}

	public Optional<Product> getById(Long id) {
		return productRepository.findById(id);
	}

	public List<Product> getAllById(Optional<List<Long>> ids) {
		return productRepository.findAllById(ids.orElse(Collections.emptyList()));
	}
	
	public List<Product> findAll(){
		return productRepository.findAll();
	}
	
	public List<Product> getUserProducts(WallapopUser user){
		return productRepository.findByUser(user);
	}
	
	public List<Product> getPurchaseProducts(Purchase purchase){
		return productRepository.findByPurchase(purchase);
	}
	
	public List<Product> unsoldProducts(){
		return productRepository.findByPurchaseIsNull();
	}

	public List<Product> unsoldProductsSearch(String name) {
		return productRepository.findByPurchaseIsNullAndNameContainingIgnoreCase(name);
	}

	public List<Product> userProductsSearch(String name, WallapopUser user) {
		return productRepository.findByUserAndNameContainingIgnoreCase(user, name);
	}
	
	public Product addToPurchase(Product product, Purchase purchase) {
		product.setPurchase(purchase);
		return productRepository.save(product);
	}
}
