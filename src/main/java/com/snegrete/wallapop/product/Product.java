package com.snegrete.wallapop.product;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.validation.constraints.Min;

import com.snegrete.wallapop.pruchase.Purchase;
import com.snegrete.wallapop.user.WallapopUser;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data @NoArgsConstructor
@Entity
public class Product {
	
	@Id @GeneratedValue @Min(0)
	private Long id;
	
	private String name;
	
	private Float price;
	
	private String image;
	
	@ManyToOne
	@JoinColumn(name = "USER_ID")
	private WallapopUser user;
	
	@ManyToOne
	@JoinColumn(name = "PURCHASE_ID")
	private Purchase purchase;

	public Product(String name, Float price, String image) {
		super();
		this.name = name;
		this.price = price;
		this.image = image;
	}
}
