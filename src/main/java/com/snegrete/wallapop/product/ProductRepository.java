package com.snegrete.wallapop.product;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.snegrete.wallapop.pruchase.Purchase;
import com.snegrete.wallapop.user.WallapopUser;

public interface ProductRepository extends JpaRepository<Product, Long> {

	List<Product> findByUser(WallapopUser user);

	List<Product> findByPurchase(Purchase purchase);

	List<Product> findByPurchaseIsNull();
	
	List<Product> findByPurchaseIsNullAndNameContainingIgnoreCase(String name);

	List<Product> findByUserAndNameContainingIgnoreCase(WallapopUser user, String name);
}
