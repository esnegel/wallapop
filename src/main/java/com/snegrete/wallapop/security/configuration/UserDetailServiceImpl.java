package com.snegrete.wallapop.security.configuration;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.snegrete.wallapop.user.WallapopUser;
import com.snegrete.wallapop.user.WallapopUserRepository;

@Service
public class UserDetailServiceImpl implements UserDetailsService {

	@Autowired
	private WallapopUserRepository userRepository;
	
	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {		
		return buildUser(findUser(username));
	}
	
	private WallapopUser findUser(String email) {	
		return userRepository.findByEmail(email).orElseThrow(() -> new UsernameNotFoundException("Usuario incorrecto"));
	}
	
	private UserDetails buildUser(WallapopUser user) {
		return User.withUsername(user.getEmail())
				.disabled(false)
				.password(user.getPassword())
				.authorities(new SimpleGrantedAuthority("ROLE_USER")).build();
	}

}
