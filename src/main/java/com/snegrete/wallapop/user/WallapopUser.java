package com.snegrete.wallapop.user;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Email;
import javax.validation.constraints.Min;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;
import org.hibernate.validator.constraints.UniqueElements;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import com.snegrete.wallapop.product.Product;
import com.snegrete.wallapop.pruchase.Purchase;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data @NoArgsConstructor
@Entity(name="USER") @EntityListeners(AuditingEntityListener.class)
public class WallapopUser {
	
	@Id @GeneratedValue @Min(0)
	private Long id;
	
	private String name;
	
	private String surname;
	
	private String avatar;
	
	@CreatedDate
	@Temporal(TemporalType.TIMESTAMP)
	private Date signUpDate;
	
	@Email @Column(unique = true)
	private String email;
	
	private String password;
	
	@OneToMany(mappedBy = "user")
	private List<Purchase> purchases;

	@OneToMany(mappedBy = "user") @Cascade(CascadeType.ALL)
	private List<Product> products;

	public WallapopUser(String name, String surname, String avatar, @Email @UniqueElements String email, String password) {
		super();
		this.name = name;
		this.surname = surname;
		this.avatar = avatar;
		this.email = email;
		this.password = password;
	}

	public void addPurchase(Purchase purchase) {
		if(purchases == null) {
			purchases = new ArrayList<Purchase>();
		}
		purchases.add(purchase);
	}
}
