package com.snegrete.wallapop.user;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

@Service
public class WallapopUserService {

	@Autowired
	private WallapopUserRepository wallapopUserRepository;
	
	@Autowired
	private BCryptPasswordEncoder passwordEncoder;
	
	public WallapopUser signUp(WallapopUser user) {
		user.setPassword(passwordEncoder.encode(user.getPassword()));
		return wallapopUserRepository.save(user);
	}
	
	public WallapopUser getById(Long id) {
		return wallapopUserRepository.findById(id).orElse(null);
	}
	
	public WallapopUser findByEmail(String email) {
		return wallapopUserRepository.findByEmail(email).orElse(null);
	}
}
