package com.snegrete.wallapop.user;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

public interface WallapopUserRepository extends JpaRepository<WallapopUser, Long> {

	Optional<WallapopUser> findByEmail(String email);
}
