package com.snegrete.wallapop.user;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import com.snegrete.wallapop.filer.StorageService;

@Controller
public class LoginController {

	@Autowired
	private WallapopUserService wallapopUserService;
	
	@Autowired
	private StorageService storageService;
	
	@GetMapping("/")
	public String welcome() {
		return "redirect:/public/";
	}
	
	@PostMapping("/auth/signUp")
	public String signUp(@ModelAttribute WallapopUser user,
			@RequestParam(name = "file", required = false) Optional<MultipartFile> fileOpt) {
		fileOpt.ifPresent(file -> user.setAvatar(storageService.store(file)));
		wallapopUserService.signUp(user);
		return "redirect:/auth/login";
	}
	
	@GetMapping("/auth/login")
	public String login(Model model) {
		model.addAttribute("user", new WallapopUser());
		return "login";
	}
	
}
