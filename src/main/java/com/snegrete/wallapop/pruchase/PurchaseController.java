package com.snegrete.wallapop.pruchase;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import com.snegrete.wallapop.user.WallapopUser;
import com.snegrete.wallapop.user.WallapopUserService;

@Controller
@RequestMapping("/app")
public class PurchaseController {
	
	@Autowired
	private WallapopUserService wallapopUserService;

	@Autowired
	private PurchaseService purchaseService;
	
	private WallapopUser wallapopUser;
	
	@ModelAttribute(name="userPurchases")
	public List<Purchase> userPurchases(){
		wallapopUser = wallapopUserService.findByEmail(SecurityContextHolder.getContext().getAuthentication().getName());
		return wallapopUser.getPurchases();
	}
	
	@GetMapping("/my-purchases")
	public String myPurchases() {
		return "/app/purchase/purchase-list";
	}
	
	@GetMapping("/purchase/bill/{id}")
	public String bill(Model model, @PathVariable Long id) {
		Purchase purchase = purchaseService.getById(id);
		model.addAttribute("purchase", purchase);
		model.addAttribute("totalAmount", purchase.getProducts().stream().mapToDouble(p -> p.getPrice()).sum());
		return "/app/purchase/bill";
	}
}
