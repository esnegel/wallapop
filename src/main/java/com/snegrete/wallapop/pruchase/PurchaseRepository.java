package com.snegrete.wallapop.pruchase;

import java.util.Collection;

import org.springframework.data.jpa.repository.JpaRepository;

import com.snegrete.wallapop.user.WallapopUser;

public interface PurchaseRepository extends JpaRepository<Purchase, Long> {

	Collection<Purchase> findByUser(WallapopUser user);
}
