package com.snegrete.wallapop.pruchase;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import com.snegrete.wallapop.product.Product;
import com.snegrete.wallapop.product.ProductService;
import com.snegrete.wallapop.user.WallapopUser;
import com.snegrete.wallapop.user.WallapopUserService;

@Controller
@RequestMapping("/app")
public class ShoppingCartController {
	
	@Autowired
	private WallapopUserService wallapopUserService;

	@Autowired
	private PurchaseService purchaseService;
	
	@Autowired
	private ProductService productService;

	@Autowired
	private HttpSession session;
	
	@ModelAttribute(name = "shoppingCart")
	public List<Product> shoppingCart() {
		return productService.getAllById(Optional.ofNullable((List<Long>)session.getAttribute("shoppingCart")));
	}
	
	@ModelAttribute(name = "totalAmount")
	public Double totalAmount() {
		return shoppingCart().stream().mapToDouble(p -> p.getPrice()).sum();
	}
	
	@GetMapping("/shopping-cart")
	public String showShoppingCart() {
		return "/app/purchase/shopping-cart";
	}
	
	@GetMapping("/shopping-cart/add/{id}")
	public String addProductToShoppingCart(@PathVariable Long id) {
		List<Long> cart = Optional.ofNullable((List<Long>)session.getAttribute("shoppingCart")).orElse(new ArrayList<Long>());
		cart.add(id);
		session.setAttribute("shoppingCart", cart);
		return "redirect:/app/shopping-cart";
	}
	
	@GetMapping("/shopping-cart/delete/{id}")
	public String deleteProductFromShoppingCart(@PathVariable Long id) {
		session.setAttribute("shoppingCart", 
				((List<Long>)session.getAttribute("shoppingCart"))
					.stream().filter(pid -> !pid.equals(id)).collect(Collectors.toList()));
		return "redirect:/app/shopping-cart";
	}
	
	@GetMapping("/shopping-cart/buy")
	public String buy() {
		Purchase purchase = new Purchase();
		shoppingCart().forEach(p -> p.setPurchase(purchase));
		purchase.setProducts(shoppingCart());
		
		WallapopUser wallapopUser = wallapopUserService.findByEmail(SecurityContextHolder.getContext().getAuthentication().getName());
		purchase.setUser(wallapopUser);
		wallapopUser.addPurchase(purchase);
		
		purchaseService.buy(purchase);
		session.setAttribute("shoppingCart", new ArrayList<Long>());
		return "redirect:/app/purchase/bill/" + purchase.getId();
	}
}
