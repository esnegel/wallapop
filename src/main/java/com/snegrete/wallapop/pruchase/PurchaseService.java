package com.snegrete.wallapop.pruchase;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.snegrete.wallapop.user.WallapopUser;

@Service
public class PurchaseService {

	@Autowired
	private PurchaseRepository purchaseRepository;
	
	public Purchase buy(Purchase purchase, WallapopUser user) {
		purchase.setUser(user);
		return purchaseRepository.save(purchase);
	}
	
	public Purchase buy(Purchase purchase) {
		return purchaseRepository.save(purchase);
	}
	
	public Purchase getById(Long id) {
		return purchaseRepository.findById(id).orElse(null);
	}
	
	public Collection<Purchase> findAll() {
		return purchaseRepository.findAll();
	}
	
	public Collection<Purchase> findByUser(WallapopUser user) {
		return purchaseRepository.findByUser(user);
	}
}
