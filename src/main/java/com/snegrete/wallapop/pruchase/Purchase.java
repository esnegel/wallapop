package com.snegrete.wallapop.pruchase;

import java.util.Collection;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.Min;

import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import com.snegrete.wallapop.product.Product;
import com.snegrete.wallapop.user.WallapopUser;

import lombok.Data;

@Data
@Entity @EntityListeners(AuditingEntityListener.class)
public class Purchase {
	
	@Id @GeneratedValue @Min(0)
	private Long id;

	@CreatedDate
	@Temporal(TemporalType.TIMESTAMP)
	private Date transactionDate;
	
	@OneToMany(mappedBy = "purchase")
	private Collection<Product> products;
	
	@OneToOne
	@JoinColumn(name = "USER_ID")
	private WallapopUser user;
}
