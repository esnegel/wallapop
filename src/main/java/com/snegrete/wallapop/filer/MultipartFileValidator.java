package com.snegrete.wallapop.filer;

import java.net.MalformedURLException;
import java.nio.file.Path;

import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import com.snegrete.wallapop.filer.exception.StorageException;
import com.snegrete.wallapop.filer.exception.StorageFileNotFoundException;

@Component
public class MultipartFileValidator {

	public String validateFileToStore(MultipartFile file) {
		String filename = StringUtils.cleanPath(file.getOriginalFilename());
		String extension = StringUtils.getFilenameExtension(filename);
		String justFilename = filename.replace("." + extension, "");
		String finalName = System.currentTimeMillis() + "_" + justFilename + "." + extension;
		if (file.isEmpty()) {
			throw new StorageException("Failed to store empty file " + filename);
		}
		if (filename.contains("..")) {
			// This is a security check
			throw new StorageException("Cannot store file with relative path outside current directory " + filename);
		}
		return finalName;	
	}
	
	public Resource load(Path filePath) {
        Resource resource;
		try {
			resource = new UrlResource(filePath.toUri());
	        if (!resource.exists() || !resource.isReadable()) {
	            throw new StorageFileNotFoundException("Could not read file: " + filePath.getFileName());
	        }
	        return resource;
		} catch (MalformedURLException e) {
			throw new StorageFileNotFoundException("Could not read file: " + filePath.getFileName(), e);
		}
	}
}
