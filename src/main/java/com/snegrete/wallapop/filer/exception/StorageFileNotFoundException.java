package com.snegrete.wallapop.filer.exception;

public class StorageFileNotFoundException extends StorageException {

	private static final long serialVersionUID = 4279308033045026757L;

	public StorageFileNotFoundException(String message) {
		super(message);
	}

	public StorageFileNotFoundException(String message, Throwable cause) {
		super(message, cause);
	}
}
