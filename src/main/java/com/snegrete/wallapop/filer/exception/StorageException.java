package com.snegrete.wallapop.filer.exception;

public class StorageException extends RuntimeException {
	
	private static final long serialVersionUID = -2226708206294013733L;

	public StorageException(String message) {
		super(message);
	}
	
	public StorageException(String message, Throwable cause) {
		super(message, cause);
	}
}
