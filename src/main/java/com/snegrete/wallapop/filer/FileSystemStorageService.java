package com.snegrete.wallapop.filer;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.stream.Stream;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;
import org.springframework.util.FileSystemUtils;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.method.annotation.MvcUriComponentsBuilder;

import com.snegrete.wallapop.filer.configuration.StorageProperties;
import com.snegrete.wallapop.filer.exception.StorageException;


/**
 * Implementación de un {@link StorageService} que almacena
 * los ficheros subidos dentro del servidor donde se ha desplegado
 * la apliacación.
 * 
 * ESTO SE REALIZA ASÍ PARA NO HACER MÁS COMPLEJO EL EJEMPLO.
 * EN UNA APLICACIÓN EN PRODUCCIÓN POSIBLEMENTE SE UTILICE
 * UN ALMACÉN REMOTO.
 *
 */
@Service
public class FileSystemStorageService implements StorageService{

	// Directorio raiz de nuestro almacén de ficheros 
	private final Path rootLocation;

		
    @Autowired
    public FileSystemStorageService(StorageProperties properties) {
        this.rootLocation = Paths.get(properties.getLocation());
    }
    
    @Autowired
    private MultipartFileValidator multipartFileValidator;

    
    /**
     * Método que almacena un fichero en el almacenamiento secundario
     * desde un objeto de tipo {@link org.springframework.web.multipart#MultipartFile} MultipartFile
     * 
     * Modificamos el original del ejemplo de Spring para cambiar el nombre
     * del fichero a almacenar. Como lo asociamos al Empleado que se ha
     * dado de alta, usaremos el ID de empleado como nombre de fichero.
     * 
     */
    @Override
    public String store(MultipartFile file) {
    	checkRootLocation();
    	return storeFile(file, multipartFileValidator.validateFileToStore(file));
    }
    
    private String storeFile(MultipartFile file, String finalName) {
        try (InputStream inputStream = file.getInputStream()) {
            Files.copy(inputStream, this.rootLocation.resolve(finalName),
                StandardCopyOption.REPLACE_EXISTING);
        } catch (IOException e) {
            throw new StorageException("Failed to store file " + finalName, e);
        }
        return MvcUriComponentsBuilder.fromMethodName(FilerController.class, "serveFile", finalName)
        		.build().toUriString();
    }
    private void checkRootLocation() {
    	File root = new File(rootLocation.toUri());
    	if(!root.exists()) {
    		root.mkdir();
    	}
    }

    /**
     * Método que devuelve la ruta de todos los ficheros que hay
     * en el almacenamiento secundario del proyecto.
     */
    @Override
    public Stream<Path> loadAll() {
        try {
            return Files.walk(this.rootLocation, 1)
                .filter(path -> !path.equals(this.rootLocation))
                .map(this.rootLocation::relativize);
        }
        catch (IOException e) {
            throw new StorageException("Failed to read stored files", e);
        }

    }

    /**
     * Método que es capaz de cargar un fichero a partir de su nombre
     * Devuelve un objeto de tipo Path
     */
    @Override
    public Path load(String filename) {
        return rootLocation.resolve(filename);
    }

    
    /**
     * Método que es capaz de cargar un fichero a partir de su nombre
     * Devuelve un objeto de tipo Resource
     */
    @Override
    public Resource loadAsResource(String filename) {
        return multipartFileValidator.load(load(filename));
    }

    
    /**
     * Método que elimina todos los ficheros del almacenamiento
     * secundario del proyecto.
     */
    @Override
    public void deleteAll() {
        FileSystemUtils.deleteRecursively(rootLocation.toFile());
    }

    
    /**
     * Método que inicializa el almacenamiento secundario del proyecto
     */
    @Override
    public void init() {
        try {
            Files.createDirectories(rootLocation);
        }
        catch (IOException e) {
            throw new StorageException("Could not initialize storage", e);
        }
    }


	@Override
	public void delete(String filename) {
		String justFilename = StringUtils.getFilename(filename);
		try {
			Path file = load(justFilename);
			Files.deleteIfExists(file);
		} catch (IOException e) {
			throw new StorageException("Error al eliminar un fichero", e);
		}
		
	}

}
