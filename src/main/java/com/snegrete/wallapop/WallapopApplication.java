package com.snegrete.wallapop;

import java.util.Arrays;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;

import com.snegrete.wallapop.filer.configuration.StorageProperties;
import com.snegrete.wallapop.product.Product;
import com.snegrete.wallapop.user.WallapopUser;
import com.snegrete.wallapop.user.WallapopUserService;

@SpringBootApplication
@EnableConfigurationProperties(StorageProperties.class)
public class WallapopApplication {

	public static void main(String[] args) {
		SpringApplication.run(WallapopApplication.class, args);
	}

	@Bean
	CommandLineRunner initData(WallapopUserService wallapopUserService) {
		return args -> {
			WallapopUser sergio = new WallapopUser("Sergio", "Negrete",
					"https://www.menkind.co.uk/media/catalog/product/cache/49dcd5d85f0fa4d590e132d0368d8132/7/4/74356---crash-bandicoot-7_-action-figure-save-for-web-1.jpg",
					"snegrete@gmail.com", "aaa");
			
			WallapopUser carmen = new WallapopUser("Carmen", "Muñoz",
					"https://www.menkind.co.uk/media/catalog/product/cache/49dcd5d85f0fa4d590e132d0368d8132/7/4/74356---crash-bandicoot-7_-action-figure-save-for-web-1.jpg",
					"camunoz@gmail.com", "aaa");
			
			Product amazfit = new Product("Amazfit Stratos 2", 100F, "https://thumb.pccomponentes.com/w-530-530/articles/16/160973/1.jpg");
			amazfit.setUser(sergio);
			
			Product ps4 = new Product("Play Station 4 Pro", 220F, "https://http2.mlstatic.com/play-station-4-pro-nueva-D_NQ_NP_966609-MCO28831770264_112018-Q.jpg");
			ps4.setUser(carmen);
			
			sergio.setProducts(Arrays.asList(amazfit));
			carmen.setProducts(Arrays.asList(ps4));

			wallapopUserService.signUp(sergio);
			wallapopUserService.signUp(carmen);
		};
	}
}
